import React, {useState, useEffect, useContext} from 'react';
import	{Form, Button} from "react-bootstrap";
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import {Navigate, useNavigate} from 'react-router-dom';

export default function Login(){
	const {user, setUser} = useContext(UserContext);
	const navigate = useNavigate();
	// store values of input fields
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');

	const [isActive, setIsActive] = useState(true);

	useEffect(()=>{
		if(email !=='' && password1 !== ''){
			setIsActive(true);
		} else{
			setIsActive(false);
		}
	}, [email, password1])

	
	
	function loginUser(e) {
		e.preventDefault();

		fetch('http://localhost:4000/users/login', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				email: email,
				password: password1
			})
		})
		.then(response => response.json())
		.then(data => {
			console.log(data)

			if (data.accessToken !== undefined){
				localStorage.setItem('accessToken', data.accessToken);
				localStorage.setItem('email', data.email);

				setUser({
					accessToken: data.accessToken})

				setEmail('');
				setPassword1('');
				Swal.fire({
				  title: 'Good job!',
				  text: `${data.email} Login successful!`,
				  icon:'success'
				})

				fetch('http://localhost:4000/users/getUserDetails', {
					headers: {
						Authorization: `Bearer ${data.accessToken}`
					}
				})
				.then(res=>res.json())
				.then(result=>{
					if(result.isAdmin === true){
						localStorage.setItem('email', result.email);
						localStorage.setItem('isAdmin', result.isAdmin);
						setUser({
							email: result.email,
							isAdmin: result.isAdmin

						})

						navigate('/courses')
					} else{
						navigate('/')
					}
				})

			}else{

				Swal.fire({
				  title: 'Oppss!',
				  text: `Something went wrong. Check your credentials.!`,
				  icon:'error'
				})
			}
		})	

		// 	e.preventDefault();

		
	}
		

	return (

		(user.accessToken !== null) ?
			<Navigate to="/courses" />
		:

			<Form className = "p-2" onSubmit={(e) => loginUser(e) }>
				<Form.Group>
					<h1 className = "text-center mb-2">Login</h1>
					<Form.Label>Email Address</Form.Label>
					<Form.Control 
						type="email"
						placeholder = "ex.: juan@email.com"
						required	
						value={email}
						onChange={e => setEmail(e.target.value)}
					/>
				
				</Form.Group>

				<Form.Group>
					<Form.Label>Password</Form.Label>
					<Form.Control 
						type="password"
						placeholder = "Enter password"
						required
						value={password1}
						onChange={e => setPassword1(e.target.value)}
					/>
				
				</Form.Group>	

				{isActive ?
					<Button className="mt-3" variant="primary" type="submit" > Login </Button>
					:
					<Button className="mt-3" variant="primary" type="submit" disabled> Login </Button>
				}

			</Form>
	)
}