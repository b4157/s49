import React from 'react';
import { Link } from 'react-router-dom';

export default function PageNotFound(){
  return (
    <>
      <h1>404 - Page Not Found</h1>
      Back to <Link to="/">  Homepage </Link>
    </>
  )

}