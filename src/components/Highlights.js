import React from 'react';
import	{Row, Col, Card} from "react-bootstrap";

export default function Highlights(){
	return (
		<Row>
			<Col xs={12} md={4}>
				<Card className="card-highlight p-3">
					<Card.Body>
						<Card.Title>
							<h2>Learn from Home</h2>
						</Card.Title>
						<Card.Text>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sequi perferendis aut impedit consequuntur praesentium vero possimus voluptate magnam libero cum et pariatur ullam deleniti inventore vitae itaque quod quis, dolorum.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>

			<Col xs={12} md={4}>
				<Card className="card-highlight p-3">
					<Card.Body>
						<Card.Title>
							<h2>Study Now, Pay Later</h2>
						</Card.Title>
						<Card.Text>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sequi perferendis aut impedit consequuntur praesentium vero possimus voluptate magnam libero cum et pariatur ullam deleniti inventore vitae itaque quod quis, dolorum.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>

			<Col xs={12} md={4}>
				<Card className="card-highlight p-3">
					<Card.Body>
						<Card.Title>
							<h2>Be Part of our Community</h2>
						</Card.Title>
						<Card.Text>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sequi perferendis aut impedit consequuntur praesentium vero possimus voluptate magnam libero cum et pariatur ullam deleniti inventore vitae itaque quod quis, dolorum.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
		</Row>
	)

}